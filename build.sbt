import com.typesafe.sbt.packager.docker.DockerChmodType

name := """maptiles-server"""
organization := "fr.pixime"

ThisBuild / scalaVersion := "2.13.1"

lazy val maptiles = (project in file("modules/maptiles"))
  .enablePlugins(PlayJava)
  .enablePlugins(UniversalPlugin)

lazy val root = (project in file("."))
  .enablePlugins(PlayJava)
  .settings(dockerSettings)
  .dependsOn(maptiles)
  .aggregate(maptiles)
  .settings(
    libraryDependencies ++= Seq(
      guice,
    )
  )

def dockerSettings = Seq(
  maintainer in Docker := "xavier.thomas@pixime.fr",
  aggregate in Docker := false,
  dockerBaseImage := "zuncle/openjdk8-gdal-tippecanoe-mbutil:latest",
  dockerRepository := Some("zuncle"),
  daemonUserUid in Docker := None,
  daemonUser in Docker := "root",
  //dockerUpdateLatest := true,
  dockerExposedPorts := Seq(9000),
  dockerChmodType := DockerChmodType.UserGroupWriteExecute,
  dockerExposedVolumes := Seq("/opt/docker/logs", "/opt/docker/conf", "/opt/docker/data")
)
