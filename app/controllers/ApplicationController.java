package controllers;

import com.typesafe.config.Config;
import play.Application;
import play.mvc.*;

import javax.inject.Inject;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class ApplicationController extends Controller {

    public Result index() {
        return ok("maptiles-server");
    }
}
