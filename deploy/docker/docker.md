
# run locally

    $ docker run --name maptiles-server -p 8081:9000 -e JAVA_OPTS="-Xms256m -Xmx256m -Dconfig.resource=prod-application.conf" -e ALLOWED_HOST="localhost" -v $(PWD)/data:/opt/docker/data zuncle/maptiles-server:latest

